<?php
require_once "Database.php";
require_once "DatabaseObject.php";

class Room implements DatabaseObject
{
    private $id;
    private $number;
    private $name;
    private $persons;
    private $price;
    private $balcony;

    private $errors = [];


    public function create()
    {
        $db = Database::connect();
        $query = 'INSERT INTO rooms (number, name, persons, price, balcony) VALUES (?,?,?,?,?)';
        $stmt = $db->prepare($query);
        $stmt->execute(array($this->number, $this->name, $this->persons, $this->price, $this->balcony));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        $this->id = $lastId;
        return $lastId;

    }

    public function update()
    {
        $db = Database::connect();
        $query = 'UPDATE rooms SET number = ?, name = ?, persons = ?, price = ?, balcony = ? WHERE id = ?';
        $stmt = $db->prepare($query);
        $stmt->execute(array($this->number, $this->name, $this->persons, $this->price, $this->balcony, $this->id));
        Database::disconnect();
    }

    public static function get($id)
    {
        $db = Database::connect();
        $query = 'SELECT * FROM rooms WHERE id = ?';
        $stmt = $db->prepare($query);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Room');
        Database::disconnect();
        return $item ?? null;
    }

    public static function getAll()
    {
        $db = Database::connect();
        $query = 'SELECT * FROM rooms ORDER BY id ASC';
        $stmt = $db->prepare($query);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Room');
        Database::disconnect();
        return $items ?? null;

    }

    public static function delete($id)
    {
        $db = Database::connect();
        $query = 'DELETE FROM rooms WHERE id=:id';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        Database::disconnect();
    }

    public function validateRoom(){
        return $this->validateNumber() & $this->validateName() & $this->validatePersons()
            & $this->validatePrice();
    }

    public function validateRoomUpdate(){
        return $this->validateNumberUpdate() & $this->validateName() & $this->validatePersons()
            & $this->validatePrice();
    }

    public function validateNumber(){
        if(is_int($this->number)) {
            $this->errors['number'] = "Nur Ganzzahlen als Zimmernummer erlaubt";
            return false;
        }elseif (strlen($this->number) > 4){
            $this->errors['number'] = "Zimmernummer zu lange!";
            return false;
        }elseif (($this->number) == null){
            $this->errors['number'] = "Zimmernummer darf nicht leer sein";
            return false;
        }elseif ($this->checkRoomNumber($this->number) != null & $this->number != $this->checkRoomNumber($this->number) ){
            $this->errors['number'] = "Zimmernummer schon vorhanden!";
            return false;
        }else{
            return true;
        }
    }

    public function validateNumberUpdate(){
        if(is_int($this->number)) {
            $this->errors['number'] = "Nur Ganzzahlen als Zimmernummer erlaubt";
            return false;
        }elseif (strlen($this->number) > 4){
            $this->errors['number'] = "Zimmernummer zu lange!";
        }elseif (($this->number) == null){
            $this->errors['number'] = "Zimmernummer darf nicht leer sein";
        }else{
            return true;
        }
    }

    public function checkRoomNumber($roomNumber){

        $db = Database::connect();
        $query = 'SELECT rooms.number FROM rooms WHERE number = ?';
        $stmt = $db->prepare($query);
        $stmt->execute(array($roomNumber));
        $item =  $stmt->fetch();
        Database::disconnect();
        return $item ?? null;
    }

    public function validateName(){
        if(strlen($this->name) == 0){
            $this->errors['name'] = "Zimmername darf nicht leer sein!";
            return false;
        }else if(strlen($this->name) > 64){
            $this->errors['name'] = "Zimmername zu lang";
            return false;
        }else{
            return true;
        }
    }

    public function validatePersons(){
        if (($this->persons) < 1){
            $this->errors['persons']="Die Anzahl der Personen darf nicht kleiner 1 betragen";
            return false;
        }
        elseif (($this->persons) > 6) {
            $this->errors['persons'] = "Maximal 6 Personen erlaubt!";
            return false;
        }elseif (!is_int($this->persons)){
            $this->errors['persons'] = "Nur Ganzzahlen  erlaubt ";
            return false;
        }else{
            return true;
        }
    }

    public function validatePrice(){
        if(!is_numeric($this->price)){
            $this->errors['price'] = "Der Preis muss Nummerisch sein und Komma als Punkt angeben!";
            return false;
        }elseif (($this->price) == null){
            $this->errors['price'] = "Preis darf nicht leer sein";
            return false;
        }else{
            return true;
        }
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * @param mixed $persons
     */
    public function setPersons($persons): void
    {
        $this->persons = $persons;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getBalcony()
    {
        return $this->balcony;
    }

    /**
     * @param mixed $balcony
     */
    public function setBalcony($balcony): void
    {
        $this->balcony = $balcony;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number): void
    {
        $this->number = $number;
    }

    public function getErrors(){
        return $this->errors;
    }

    public function hasErrors($field){
        return isset($this->errors[$field]);
    }

}