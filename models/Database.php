<?php

class Database
{
    public static $dbName = 'zimmerreservierung';
    public static $dbHost = '192.168.227.21';
    public static $dbUsername = 'root';
    public static $dbUserPassword = '123';

    private static $conn = null;

    public function __construct()
    {
        exit('Init function is not allowed');
    }

    public static function connect()
    {
        // One connection through whole application
        if (null == self::$conn) {
            try {
                // Verbindung zur MySQL-Datenbank mittels PDO (PHP Database Object)
                self::$conn = new PDO("mysql:host=" . self::$dbHost . ";" . "dbname=" . self::$dbName, self::$dbUsername, self::$dbUserPassword);
            } catch (PDOException $e) {
//                die($e->getMessage());
                return null;
            }
        }

        //Legt ein Attribut für das Datenbank-Handling fest. (Fehler melden (Error reporting), Ausnahmen auslösen Throw exeptions )
        self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$conn;
    }

    public static function disconnect()
    {
        self::$conn = null;
    }

    public static function databaseCheck()
    {
        if (Database::connect() == null) {
            header('Location: ../../index.php');
            die;
        }
    }
}

?>