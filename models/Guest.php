<?php
require_once "Database.php";
require_once "DatabaseObject.php";

class Guest implements DatabaseObject
{
    private $id;
    private $name;
    private $email;

    private $errors = [];


    public function create()
    {
        $db = Database::connect();
        $query = 'INSERT INTO guests (name, email) VALUES (?,?)';
        $stmt = $db->prepare($query);
        $stmt->execute(array($this->name, $this->email));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    public function update()
    {
        $db = Database::connect();
        $query = 'UPDATE guests SET name = ?, email = ? WHERE id = ?';
        $stmt = $db->prepare($query);
        $stmt->execute(array($this->name, $this->email, $this->id));
        Database::disconnect();
    }

    public static function get($id)
    {
        $db = Database::connect();
        $query = 'SELECT * FROM guests WHERE id = ?';
        $stmt = $db->prepare($query);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Guest');
        Database::disconnect();
        return $item ?? null;
    }

    public static function getAll()
    {
        $db = Database::connect();
        $query = 'SELECT * FROM guests ORDER BY id ASC';
        $stmt = $db->prepare($query);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Guest');
        Database::disconnect();
        return $items ?? null;
    }

    public static function delete($id)
    {
        $db = Database::connect();
        $query = 'DELETE FROM guests WHERE id=:id';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        Database::disconnect();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getErrors(){
        return $this->errors;
    }

    public function hasErrors($field){
        return isset($this->errors[$field]);
    }

    public function validateGuest(){
        return $this->validateName() & $this->validateEmail();
    }

    public function validateName(){
        if (strlen($this->name)== 0){
            $this->errors['name'] = "Name darf nicht leer sein!";
            return false;
        } elseif (strlen($this->name) > 20){
            $this->errors['name'] = "Name zu lang";
            return false;
        }else{
            return true;
        }
    }

    public function validateEmail(){

        if ($this->email != "" && !filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->errors['email'] = "E-Mail ungültig";
            return false;
        } else {
            return true;
        }
    }

}