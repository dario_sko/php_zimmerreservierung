<?php
require_once "Database.php";
require_once "DatabaseObject.php";

class Reservation implements DatabaseObject
{
    private $id;
    private $from_date;
    private $to_date;
    private $guests_id;
    private $rooms_id;

    private $errors = [];

    public function create()
    {
        $db = Database::connect();
        $query = 'INSERT INTO reservations (from_date, to_date, guests_id, rooms_id) VALUES (?,?,?,?)';
        $stmt = $db->prepare($query);
        $stmt->execute(array($this->from_date, $this->to_date, $this->guests_id, $this->rooms_id));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;

    }

    public function update()
    {
        $db = Database::connect();
        $query = 'UPDATE reservations SET from_date = ?, to_date = ?, guests_id = ?, rooms_id = ? WHERE id = ?';
        $stmt = $db->prepare($query);
        $stmt->execute(array($this->from_date, $this->to_date, $this->guests_id, $this->rooms_id, $this->id));
        Database::disconnect();
    }

    public static function get($id)
    {
        $db = Database::connect();
        $query = 'SELECT * FROM reservations WHERE id = ?';
        $stmt = $db->prepare($query);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Reservation');
        Database::disconnect();
        return $item ?? null;
    }

    public static function getAll()
    {
        $db = Database::connect();
        $query = 'SELECT * FROM reservations ORDER BY id ASC';
        $stmt = $db->prepare($query);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Reservation');
        Database::disconnect();
        return $items ?? null;
    }

    public static function delete($id)
    {
        $db = Database::connect();
        $query = 'DELETE FROM reservations WHERE id=:id';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        Database::disconnect();
    }

    public function getGuestName()
    {
        $db = Database::connect();
        $query = 'SELECT guests.name
                    FROM reservations
                    JOIN guests ON reservations.guests_id = guests.id
                    AND reservations.guests_id = ?;';
        $stmt = $db->prepare($query);
        $stmt->execute(array($this->guests_id));
        $item = $stmt->fetch();
        Database::disconnect();
        return $item['name'] ?? null;
    }

    public function getRoomName()
    {
        $db = Database::connect();
        $query = 'SELECT rooms.name
                    FROM reservations
                    JOIN rooms ON reservations.rooms_id = rooms.id
                    AND reservations.rooms_id = ?;';
        $stmt = $db->prepare($query);
        $stmt->execute(array($this->rooms_id));
        $item = $stmt->fetch();
        Database::disconnect();
        return $item['name'] ?? null;
    }

    public function getTotalPrice()
    {
        $db = Database::connect();
        $query = 'SELECT rooms.price
                    FROM reservations
                    JOIN rooms ON reservations.rooms_id = rooms.id
                    AND reservations.rooms_id = ?;';
        $stmt = $db->prepare($query);
        $stmt->execute(array($this->rooms_id));
        $price = $stmt->fetch();
        $date1 = new DateTime($this->from_date);
        $date2 = new DateTime($this->to_date);
        $days = $date1->diff($date2);
        $totalPrice = $days->days*$price['price'];
        return number_format($totalPrice,2) ?? null;
    }

//    public static function getAllRoomNames()
//    {
//        $db = Database::connect();
//        $query = 'SELECT rooms.name, rooms.id
//                    FROM reservations
//                    JOIN rooms ON reservations.rooms_id = rooms.id;';
//        $stmt = $db->prepare($query);
//        $stmt->execute();
//        $item = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        Database::disconnect();
//        return $item ?? null;
//    }

//    public static function getAllGuestNames()
//    {
//        $db = Database::connect();
//        $query = 'SELECT guests.name, guests.id
//                    FROM reservations
//                    JOIN guests ON reservations.guests_id = guests.id;';
//        $stmt = $db->prepare($query);
//        $stmt->execute();
//        $item = $stmt->fetchAll(PDO::FETCH_ASSOC);
//        Database::disconnect();
//        return $item ?? null;
//    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFromDate()
    {
        return $this->from_date;
    }

    /**
     * @param mixed $from_date
     */
    public function setFromDate($from_date): void
    {
        $this->from_date = $from_date;
    }

    /**
     * @return mixed
     */
    public function getToDate()
    {
        return $this->to_date;
    }

    /**
     * @param mixed $to_date
     */
    public function setToDate($to_date): void
    {
        $this->to_date = $to_date;
    }

    /**
     * @return mixed
     */
    public function getGuestsId()
    {
        return $this->guests_id;
    }

    /**
     * @param mixed $guests_id
     */
    public function setGuestsId($guests_id): void
    {
        $this->guests_id = $guests_id;
    }

    /**
     * @return mixed
     */
    public function getRoomsId()
    {
        return $this->rooms_id;
    }

    /**
     * @param mixed $rooms_id
     */
    public function setRoomsId($rooms_id): void
    {
        $this->rooms_id = $rooms_id;
    }

    public function validateReservation(){
        return  $this->validateFromDate() & $this->validateToDate() & $this->validateGuestId() & $this->validateRoomId() ;
    }

    public function validateGuestId(){
        if($this->guests_id == null){
            $this->errors['guests_id'] = "Sie müssen einen Gast auswählen";
            return false;
        }
        return true;
    }

    public function validateRoomId(){
        if($this->rooms_id == null){
            $this->errors['rooms_id'] = "Sie müssen ein Zimmer auswählen";
            return false;
        }
        return true;
    }

    public function validateFromDate(){
        try{
            if($this->from_date == '') {
                $this->errors['from_date'] = "Startdatum darf nicht leer sein!";
                return false;
            }elseif (new DateTime($this->from_date) < new DateTime()){
                $this->errors['from_date'] = "Startdatum darf nicht in der Vergangenheit liegen!";
                return false;
            }
            return true;
        }catch (Exception $exception){
            $this->errors['from_date'] = "Startdatum ungültig";
            return false;
        }
    }

    public function validateToDate(){
        try{
            if($this->to_date == '') {
                $this->errors['to_date'] = "Enddatum darf nicht leer sein!";
                return false;
            }elseif (new DateTime($this->to_date) < new DateTime()){
                $this->errors['to_date'] = "Enddatum darf nicht in der Vergangenheit liegen!";
                return false;
            }
            return true;
        }catch (Exception $exception){
            $this->errors['to_date'] = "Enddatum ungültig";
            return false;
        }
    }

    public function getErrors(){
        return $this->errors;
    }

    public function hasErrors($field){
        return isset($this->errors[$field]);
    }

}