-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema zimmerreservierung
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema zimmerreservierung
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `zimmerreservierung` DEFAULT CHARACTER SET utf8 ;
USE `zimmerreservierung` ;

-- -----------------------------------------------------
-- Table `zimmerreservierung`.`guests`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zimmerreservierung`.`guests` ;

CREATE TABLE IF NOT EXISTS `zimmerreservierung`.`guests` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zimmerreservierung`.`rooms`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zimmerreservierung`.`rooms` ;

CREATE TABLE IF NOT EXISTS `zimmerreservierung`.`rooms` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `number` INT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `persons` INT NOT NULL,
  `price` DOUBLE NOT NULL,
  `balcony` TINYINT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zimmerreservierung`.`reservations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zimmerreservierung`.`reservations` ;

CREATE TABLE IF NOT EXISTS `zimmerreservierung`.`reservations` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `from_date` DATE NOT NULL,
  `to_date` DATE NOT NULL,
  `guests_id` INT NOT NULL,
  `rooms_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_reservations_guests_idx` (`guests_id` ASC) VISIBLE,
  INDEX `fk_reservations_rooms1_idx` (`rooms_id` ASC) VISIBLE,
  CONSTRAINT `fk_reservations_guests`
    FOREIGN KEY (`guests_id`)
    REFERENCES `zimmerreservierung`.`guests` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_reservations_rooms`
    FOREIGN KEY (`rooms_id`)
    REFERENCES `zimmerreservierung`.`rooms` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `zimmerreservierung`.`guests`
-- -----------------------------------------------------
START TRANSACTION;
USE `zimmerreservierung`;
INSERT INTO `zimmerreservierung`.`guests` (`id`, `name`, `email`) VALUES (1, 'Clemens Kerber', 'kerber@kerber.at');
INSERT INTO `zimmerreservierung`.`guests` (`id`, `name`, `email`) VALUES (2, 'Dario Sko', 'sko@sko.at');
INSERT INTO `zimmerreservierung`.`guests` (`id`, `name`, `email`) VALUES (3, 'Mathias Rudig', 'rudig@rudig.at');
INSERT INTO `zimmerreservierung`.`guests` (`id`, `name`, `email`) VALUES (4, 'Tobias Tilg', 'tilg@tilg.at');
INSERT INTO `zimmerreservierung`.`guests` (`id`, `name`, `email`) VALUES (5, 'Clemens Zurcük', 'zurcük@zurük.at');

COMMIT;


-- -----------------------------------------------------
-- Data for table `zimmerreservierung`.`rooms`
-- -----------------------------------------------------
START TRANSACTION;
USE `zimmerreservierung`;
INSERT INTO `zimmerreservierung`.`rooms` (`id`, `number`, `name`, `persons`, `price`, `balcony`) VALUES (1, 1, 'Tres-Zap', 4, 53.72, false);
INSERT INTO `zimmerreservierung`.`rooms` (`id`, `number`, `name`, `persons`, `price`, `balcony`) VALUES (2, 2, 'Pannier', 2, 53.72, false);
INSERT INTO `zimmerreservierung`.`rooms` (`id`, `number`, `name`, `persons`, `price`, `balcony`) VALUES (3, 3, 'Gembucket', 1, 81.87, true);
INSERT INTO `zimmerreservierung`.`rooms` (`id`, `number`, `name`, `persons`, `price`, `balcony`) VALUES (4, 4, 'Konklab', 4, 99.81, false);
INSERT INTO `zimmerreservierung`.`rooms` (`id`, `number`, `name`, `persons`, `price`, `balcony`) VALUES (5, 5, 'Zontrax', 1, 77.96, true);
INSERT INTO `zimmerreservierung`.`rooms` (`id`, `number`, `name`, `persons`, `price`, `balcony`) VALUES (6, 6, 'Veribet', 1, 85.50, false);
INSERT INTO `zimmerreservierung`.`rooms` (`id`, `number`, `name`, `persons`, `price`, `balcony`) VALUES (7, 7, 'Zoolab', 1, 36.51, false);
INSERT INTO `zimmerreservierung`.`rooms` (`id`, `number`, `name`, `persons`, `price`, `balcony`) VALUES (8, 8, 'Keylex', 3, 64.09, true);
INSERT INTO `zimmerreservierung`.`rooms` (`id`, `number`, `name`, `persons`, `price`, `balcony`) VALUES (9, 9, 'Regrant', 2, 43.35, false);
INSERT INTO `zimmerreservierung`.`rooms` (`id`, `number`, `name`, `persons`, `price`, `balcony`) VALUES (10, 10, 'Namfix', 3, 93.03, true);

COMMIT;

