<?php
require_once "../models/Database.php";

$conn = null;

// Verbindung zur Datenbank
try {
    $conn = new PDO("mysql:host=" . Database::$dbHost, Database::$dbUsername, Database::$dbUserPassword);
} catch (PDOException $e) {
    echo "<script>alert('Verbindung zur Datenbank fehlgeschlagen')</script>";
    header("Refresh:3; ../index.php");
    die();
}

// Auslesen der SQL Datei
if (file_get_contents("zimmerreservierung.sql")) {
    $sql = file_get_contents("zimmerreservierung.sql");
} else {
    echo "<script>alert('SQL Datei nicht vorhanden!')</script>";
    header("Refresh:3; ../index.php");
    die();
}

// Ausführen der SQL Datei
$conn->exec($sql);
//$code = $conn->errorCode() == null ?? "00000";
if (strcmp($conn->errorCode(), "00000")) {
    echo "<script>alert('Ausführung der SQL Datei fehlgeschlagen')</script>";
    header("Refresh:3; ../index.php");
    die();
}

// Datenbankverbindung schließen
$conn = null;

// Weiterleitung zu index.php
header("Location: ../index.php");
