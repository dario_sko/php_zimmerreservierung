# PHP_zimmerreservierung

PHP
funktionsfähiger Prototyp für die Verwaltung von Zimmern, Gästen und Reservierungen

# Bericht Testprotokoll

# Lösungsansatz Zimmerreservierung

⇒ Die Vorlage von Model herunterladen und einrichten

⇒ ER-Diagram in der Workbench erstellen

⇒ ER-Diagram mit Daten befüllen

⇒ Exportieren

⇒ Database.php und DatabaseObject (PHP singelton Pattern)

⇒ install.php erstellen

⇒ Aufbau der models + Datenfelder + Datenbankbefehle + implements DatabaseObject

⇒ index + update + view + delete + create gestalten

---

## Interessante Code-Teile

### install.php

1. Datei einbeziehen
2. Connection auf null setzen.
3. Versuche eine Connection zur Datenbank aufzubauen, wenn Fehler wird ein Skript ausgeführt und die Seite wird nach 3 Sekunden auf die index.php Seite springen.
4. Mit file_get_contents(”zimmerreservierung.sql”) bekommen wir den Aufbau der fertigen Datenbank. Wenn wir diesen bekommen setzen wir dies in die Variable $sql, ansonsten wird wieder ein Skript ausgeführt und uns wirfts nach 3 Sekunden wirder zurück zur index.php
5. Ausführen der SQL-Datei
6. Wenn ein Error Code 0000 kommt wird dieser mit 00000 verglichen und wenn dieser Fehler kommt wird wieder ein Skript ausgeführt und refresh 3 Sekunden auf index.php
7. Datenbankverbindung schließen
8. Weiterleitung auf index.php

```php
<?php
require_once "../models/Database.php";

$conn = null;

// Verbindung zur Datenbank
try {
    $conn = new PDO("mysql:host=" . Database::$dbHost, Database::$dbUsername, Database::$dbUserPassword);
} catch (PDOException $e) {
    echo "<script>alert('Verbindung zur Datenbank fehlgeschlagen')</script>";
    header("Refresh:3; ../index.php");
    die();
}

// Auslesen der SQL Datei
if (file_get_contents("zimmerreservierung.sql")) {
    $sql = file_get_contents("zimmerreservierung.sql");
} else {
    echo "<script>alert('SQL Datei nicht vorhanden!')</script>";
    header("Refresh:3; ../index.php");
    die();
}

// Ausführen der SQL Datei
$conn->exec($sql);
// $code = $conn->errorCode() == null ?? "00000";
//Vergleich zweier Strings (Binary safe)
if (strcmp($conn->errorCode(), "00000")) {
    echo "<script>alert('Ausführung der SQL Datei fehlgeschlagen')</script>";
    header("Refresh:3; ../index.php");
    die();
}

// Datenbankverbindung schließen
$conn = null;

// Weiterleitung zu index.php
header("Location: ../index.php");
```

### Database.php

```php
//Legt ein Attribut für das Datenbank-Handling fest.
//(Fehler melden (Error reporting), Ausnahmen auslösen Throw exeptions )
        self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$conn;
```

Start ohne Datenbank

![Untitled](Bericht%20Testprotokoll/Untitled.png)

Button zum installieren und erstellen der Datenbank

![Untitled](Bericht%20Testprotokoll/Untitled%201.png)

Datenbank wurde installiert:

![Untitled](Bericht%20Testprotokoll/Untitled%202.png)

Leere Startseite ohne Reservierungen

![Untitled](Bericht%20Testprotokoll/Untitled%203.png)

- Die Hinterlegten Gäste und Zimmer werden mit hinzugefügt.

---

## Gast

### Gast erstellen:

- Mit Deaktivierung von Clientseitige Validierung:

![Untitled](Bericht%20Testprotokoll/Untitled%204.png)

- Mit richtigen Daten:

![Untitled](Bericht%20Testprotokoll/Untitled%205.png)

- Gast Bearbeiten

![Untitled](Bericht%20Testprotokoll/Untitled%206.png)

- Falsche Daten beim Bearbeiten

![Untitled](Bericht%20Testprotokoll/Untitled%207.png)

- Perfekte Eingabe

![Untitled](Bericht%20Testprotokoll/Untitled%208.png)

- Änderung in Tabelle (Datenbank)

![Untitled](Bericht%20Testprotokoll/Untitled%209.png)

- Bei Gast löschen kommt Bestätigung

![Untitled](Bericht%20Testprotokoll/Untitled%2010.png)

---

## Reservierung

- Reservierung erstellen

![Untitled](Bericht%20Testprotokoll/Untitled%2011.png)

- Fehlerhafte Eingaben

![Untitled](Bericht%20Testprotokoll/Untitled%2012.png)

- Richtige Eingabe Reservierung wurde erstellt.

![Untitled](Bericht%20Testprotokoll/Untitled%2013.png)

- Aktualisieren des Datums

![Untitled](Bericht%20Testprotokoll/Untitled%2014.png)

- Datum wurde geändert

![Untitled](Bericht%20Testprotokoll/Untitled%2015.png)

---

## Zimmer

- Zimmer anlegen mit deaktivieren Clientseitiger Validierung

![Untitled](Bericht%20Testprotokoll/Untitled%2016.png)

- erstellt mit Perfekten Daten

![Untitled](Bericht%20Testprotokoll/Untitled%2017.png)

- Ändern Zimmereinstellungen

![Untitled](Bericht%20Testprotokoll/Untitled%2018.png)

- Eingabe in Ordnung

![Untitled](Bericht%20Testprotokoll/Untitled%2019.png)

- Löschen eines Zimmers

![Untitled](Bericht%20Testprotokoll/Untitled%2020.png)