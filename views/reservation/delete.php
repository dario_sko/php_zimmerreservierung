<?php
$title = "Reservierung löschen";
include '../layouts/top.php';

require_once "../../models/Reservation.php";
require_once "../../models/Guest.php";

//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();

$id = !empty($_GET['id']) ? $_GET['id'] : 0;

if (!empty($_POST['id'])) {
    Reservation::delete($_POST['id']);
    header("Location: ./index.php");
    exit();
} else {
    $re = Reservation::get($id);
}

?>

    <div class="container">
        <h2><?= $title ?></h2>

        <form class="form-horizontal" action="delete.php?id= <?= $re->getId() ?>" method="post">
            <input type="hidden" name="id" value="<?= $re->getId() ?>"/>
            <p class="alert alert-error">Wollen Sie die
                Reservierung <?= $re->getId() . " mit dem Gast " . $re->getGuestName()
                . " auf dem Zimmer " . $re->getRoomName() ?> wirklich löschen?</p>
            <div class="form-actions">
                <button type="submit" name="submit" class="btn btn-danger">Löschen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>