<?php
$title = "Reservierungen";
include '../layouts/top.php';

//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();

?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success">Erstellen <span
                            class="glyphicon glyphicon-plus"></span></a>
            </p>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Gast</th>
                    <th>Zimmer</th>
                    <th>Anreise</th>
                    <th>Abreise</th>
                    <th>Gesamtpreis</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                require_once '../../models/Reservation.php';

                $reservations = Reservation::getAll();

                foreach ($reservations as $re) {

                    echo '<tr>';
                    echo '<td> ' . $re->getId() . ' </td>';
                    echo '<td> ' . $re->getGuestName() . ' </td>';
                    echo '<td> ' . $re->getRoomName() . ' </td>';
                    echo '<td> ' . $re->getFromDate() . ' </td>';
                    echo '<td> ' . $re->getToDate() . ' </td>';
                    echo '<td> ' . $re->getTotalPrice() . '€ </td>';
                    echo '<td><a class="btn btn-info" href="view.php?id= ' . $re->getId() . ' "><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a
                                class="btn btn-primary" href="update.php?id= ' . $re->getId() . ' "><span
                                    class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a
                                class="btn btn-danger" href="delete.php?id= ' . $re->getId() . '"><span
                                    class="glyphicon glyphicon-remove"></span></a>';
                    echo '</td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>