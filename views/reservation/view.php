<?php
$title = "Reservierung anzeigen";
include '../layouts/top.php';
require_once '../../models/Reservation.php';


//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();

if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} elseif (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    $re = Reservation::get($_GET['id']);
}

if ($re == null) {
    http_response_code(404);
    die();
}
?>

    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id=<?= $re->getId() ?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?= $re->getId() ?>">Löschen</a>
            <a class="btn btn-default" href="index.php">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th>ID</th>
                <td><?= $re->getId() ?></td>
            </tr>
            <tr>
                <th>Gast</th>
                <td><?= $re->getGuestName() ?></td>
            </tr>
            <tr>
                <th>Zimmer</th>
                <td><?= $re->getRoomName() ?></td>
            </tr>
            <tr>
                <th>Anreise</th>
                <td><?= $re->getFromDate() ?></td>
            </tr>
            <tr>
                <th>Abreise</th>
                <td><?= $re->getToDate() ?></td>
            </tr>
            <tr>
                <th>Preis</th>
                <td><?= $re->getTotalPrice() ?></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>