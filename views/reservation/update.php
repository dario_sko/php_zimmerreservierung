<?php
$title = "Reservierung bearbeiten";
include '../layouts/top.php';

//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();

require_once '../../models/Reservation.php';
require_once '../../models/Room.php';
require_once '../../models/Guest.php';

$re = null;
$m = '';

if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} elseif (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    $re = Reservation::get($_GET['id']);
}

if ($re == null) {
    http_response_code(404);
    die();
}


if (!empty($_POST)) {

    $re->setGuestsId($_POST['guestid'] ?? '');
    $re->setRoomsId($_POST['roomid'] ?? '');
    $re->setFromDate($_POST['fromdate'] ?? '');
    $re->setToDate($_POST['todate'] ?? '');

    if ($re->validateReservation()) {
        $m = "<p class='alert alert-success'>Die eingegebenen Daten sind in Ordnung!</p>";
        if ($re->update()) {
            header("Location: update.php?id= " . $re->getId());
            exit();
        }
    } else {
        $m = "<div class='alert alert-danger'><p>Die eingegebenen Daten sind Falsch!</p><ul>";
        foreach ($re->getErrors() as $key => $value) {
            $m .= "<li>" . $value . "</li>";
        }
        $m .= "</ul></div>";
    }
}

$roomNames = Room::getAll();
$guestNames = Guest::getAll();

?>

<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
        <?php echo $m ?>
    </div>

    <form class="form-horizontal" action="update.php?id= <?= $re->getId() ?>" method="post">

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label ">Gast *</label>
                    <select name="guestid" class="form-control <?= $re->hasErrors('guestsid') ? 'is-invalid' : '' ?>">
                        <?php
                        foreach ($guestNames as $guestName) {
                            if ($re->getGuestsId() == $guestName->getId()) {
                                echo '<option value=' . htmlspecialchars($re->getGuestsId()) . ' selected>' . htmlspecialchars($re->getGuestName()) . '</option>';
                            } else {
                                echo '<option value=' . htmlspecialchars($guestName->getId()) . '>' . htmlspecialchars($guestName->getName()) . '</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group required ">
                    <label class="control-label">Zimmer *</label>
                    <select name="roomid" class="form-control <?= $re->hasErrors('roomsid') ? 'is-invalid' : '' ?>">
                        <?php
                        foreach ($roomNames as $roomName) {
                            if ($re->getRoomsId() == $roomName->getId()) {
                                echo '<option value=' . htmlspecialchars($re->getRoomsId()) . ' selected>' . htmlspecialchars($re->getRoomName()) . '</option>';
                            } else {
                                echo '<option value=' . htmlspecialchars($roomName->getId()) . '>' . htmlspecialchars($roomName->getName()) . '</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Anreise *</label>
                    <input type="date" class="form-control <?= $re->hasErrors(['fromdate'] ? 'is-invalid' : '') ?>"
                           name="fromdate"
                           value="<?= htmlspecialchars($re->getFromDate()) ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Abreise *</label>
                    <input type="date" class="form-control <?= $re->hasErrors(['todate'] ? 'is-invalid' : '') ?>""
                    name="todate"
                    value="<?= htmlspecialchars($re->getToDate()) ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-1">
            </div>
            <div class="col-md-5"></div>
        </div>


        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>
