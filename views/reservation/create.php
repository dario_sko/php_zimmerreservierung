<?php
$title = "Reservierung erstellen";
include '../layouts/top.php';

//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();

require_once '../../models/Reservation.php';
require_once '../../models/Room.php';
require_once '../../models/Guest.php';

$re = new Reservation();
$message = '';

$roomNames = Room::getAll();
$guestNames = Guest::getAll();

if (!empty($_POST)) {

    $re->setGuestsId($_POST['guestid'] ?? '');
    $re->setRoomsId($_POST['roomid'] ?? '');
    $re->setFromDate($_POST['fromdate'] ?? '');
    $re->setToDate($_POST['todate'] ?? '');

    if($re->validateReservation()){
        $message = "<p class='alert alert-success'>Die eingegebenen Daten sind in Ordnung!</p>";
            if ($re->create()) {
                header("Location: view.php?id= " . $re->getId());
                exit();
            }
    }else {
                $message = "<div class='alert alert-danger'><p>Die eingegebenen Daten sind Falsch!</p><ul>";
                foreach ($re->getErrors() as $key => $value) {
                    $message .= "<li>" . $value . "</li>";
                }
                $message .= "</ul></div>";
            }
}

?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
            <?php echo $message ?>
        </div>

        <form class="form-horizontal" action="create.php" method="post">

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Gast *</label>
                        <select name="guestid" class="form-control <?= $re->hasErrors('guestsid') ? 'is-invalid' : ''?>">
                            <option value="" hidden>-- Gast wählen --</option>
                            <?php
                            foreach ($guestNames as $guestName) {
                                echo '<option value=' . $guestName->getId() . '>' . $guestName->getName() . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-4">
                    <div class="form-group required ">
                        <label class="control-label">Zimmer *</label>
                        <select name="roomid" class="form-control <?= $re->hasErrors('roomsid') ? 'is-invalid' : ''?>">
                            <option value="" hidden>-- Zimmer wählen --</option>
                            <?php
                            foreach ($roomNames as $roomName) {
                                echo '<option value=' . $roomName->getId() . '>' . $roomName->getName() . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-5"></div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Anreise *</label>
                        <input type="date" class="form-control <?= $re->hasErrors(['fromdate'] ? 'is-invalid' : '')?>"
                               name="fromdate"
                               value="<?= htmlspecialchars($re->getFromDate()) ?>"
                               onchange="validateDateJS(this)" >
                    </div>
                </div>

                <div class="col-md-1"></div>
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Abreise *</label>
                        <input type="date" class="form-control <?= $re->hasErrors('todate') ? 'is-invalid' : ''?>"
                               name="todate"
                               value="<?= htmlspecialchars($re->getToDate()) ?>"
                               onchange="validateDateJS(this)" >
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-1">
                </div>
                <div class="col-md-5"></div>
            </div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-success">Erstellen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>