<?php
$title = "Zimmer löschen";
include '../layouts/top.php';

//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();

require_once "../../models/Room.php";

$id = !empty($_GET['id']) ? $_GET['id'] : 0;

if (!empty($_POST['id'])) {
    Room::delete($_POST['id']);
    header("Location: ./index.php");
    exit();
} else {
    $r = Room::get($id);
}

?>

    <div class="container">
        <h2><?= $title ?></h2>

        <form class="form-horizontal" action="delete.php?id= <?= $r->getId() ?>" method="post">
            <input type="hidden" name="id" value="<?= $r->getId() ?>"/>
            <p class="alert alert-error">Wollen Sie das Zimmer <?= $r->getName() ?> wirklich löschen?</p>
            <div class="form-actions">
                <button type="submit" name="submit" class="btn btn-danger">Löschen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>