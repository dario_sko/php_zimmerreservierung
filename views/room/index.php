<?php
$title = "Zimmerverwaltung";
include '../layouts/top.php';

//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success">Erstellen <span
                            class="glyphicon glyphicon-plus"></span></a>
            </p>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Zimmernummer</th>
                    <th>Name</th>
                    <th>Personen</th>
                    <th>Preis</th>
                    <th>Balkon</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                require_once '../../models/Room.php';

                $rooms = Room::getAll();

                foreach ($rooms as $r) {
                    echo '<tr>';
                    echo '<td> ' . $r->getNumber() . ' </td>';
                    echo '<td> ' . $r->getName() . ' </td>';
                    echo '<td> ' . $r->getPersons() . '</td>';
                    echo '<td> ' . $r->getPrice() . '€ </td>';
                    echo '<td> ' . ($r->getBalcony() ? "ja" : "nein") . ' </td>';
                    echo '<td><a class="btn btn-info" href="view.php?id= ' . $r->getId() . ' "><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a
                                class="btn btn-primary" href="update.php?id= ' . $r->getId() . ' "><span
                                    class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a
                                class="btn btn-danger" href="delete.php?id= ' . $r->getId() . '"><span
                                    class="glyphicon glyphicon-remove"></span></a>';
                    echo '</td>';
                    echo '</tr>';
                }
                ?>


                </tbody>
            </table>
        </div>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>