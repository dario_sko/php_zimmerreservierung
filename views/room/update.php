<?php
$title = "Zimmer bearbeiten";
include '../layouts/top.php';

//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();

require_once '../../models/Room.php';

$r = null;
$m = '';

if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} elseif (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    $r = Room::get($_GET['id']);
}

if ($r == null) {
    http_response_code(404);
    die();
}

if (!empty($_POST)) {

    $r->setNumber($_POST['nr'] ?? '');
    $r->setName($_POST['name'] ?? '');
    $r->setPersons(intval($_POST['persons']) ?? '');
    $r->setPrice($_POST['price'] ?? '');
    $r->setBalcony($_POST['balcony'] == 'on' ? '1' : '0');



        if($r->validateRoom()){
            $m = "<p class='alert alert-success'>Die eingegebenen Daten sind in Ordnung!</p>";
            if ($r->update()) {
                header("Location: update.php?id= " . $r->getId());
                exit();
            }
        }else{
            $m = "<div class='alert alert-danger'><p>Die eingegebenen Daten sind Falsch!</p><ul>";
            foreach ($r->getErrors() as $key => $value){
                $m .= "<li>" . $value . "</li>";
            }
            $m .= "</ul></div>";
        }
}
?>

<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
        <?php echo $m ?>
    </div>

    <form class="form-horizontal" action="update.php?id= <?= $r->getId() ?>" method="post">

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Zimmernummer *</label>
                    <input type="text" class="form-control <?= $r->hasErrors('nr') ? 'is-invalid' : ''?>"
                           name="nr"
                           maxlength="4"
                           value="<?= htmlspecialchars($r->getNumber()) ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group required ">
                    <label class="control-label">Name *</label>
                    <input type="text" class="form-control <?= $r->hasErrors('name') ? 'is-invalid' : ''?>"
                           name="name"
                           maxlength="64"
                           value="<?= htmlspecialchars($r->getName()) ?>">
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required">
                    <label class="control-label">Personen *</label>
                    <input type="number" class="form-control <?= $r->hasErrors('persons') ? 'is-invalid' : ''?>"
                           name="persons"
                           min="1"
                           value="<?= htmlspecialchars($r->getPersons()) ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Preis *</label>
                    <input type="text" class="form-control <?= $r->hasErrors('price') ? 'is-invalid' : ''?>"
                           name="price"
                           value="<?= htmlspecialchars($r->getPrice()) ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-1">
                <div class="form-group required ">
                    <label class="control-label">Balkon *</label>
                    <input type="hidden" name="balcony" value="0">
                    <input type="checkbox" class="form-control"
                           name="balcony" <?= $r->getBalcony() ? 'checked' : '' ?>>

                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>
