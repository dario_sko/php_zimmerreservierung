<?php
$title = "Zimmer erstellen";
include '../layouts/top.php';

//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();

require_once '../../models/Room.php';

$r = new Room();
$message = '';

if (!empty($_POST)) {

    $r->setNumber($_POST['nr'] ?? '');
    $r->setName($_POST['name'] ?? '');
    $r->setPersons($_POST['persons'] ?? '');
    $r->setPrice($_POST['price'] ?? '');
    $r->setBalcony($_POST['balcony'] == 'on' ? '1' : '0');


    if ($r->validateRoom()){
        $message = "<p class='alert alert-success'>Die eingegeben Daten sind in Ordnung!</p>";
        if ($r->create()) {
            header("Location: view.php?id= " . $r->getId());
            exit();
        }
    }else{
        $message = "<div class='alert alert-danger'><p>Die eingegebenen Daten sind Falsch!</p><ul>";
        foreach ($r->getErrors() as $key => $value){
            $message .= "<li>" . $value . "</li>";
        }
        $message .= "</ul></div>";
    }

}
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>

            <?php echo $message?>
        </div>

        <form class="form-horizontal" action="create.php" method="post">

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Zimmernummer *</label>
                        <input type="text" class="form-control <?=$r->hasErrors('nr') ? 'is-invalid' : ''?>"
                               name="nr"
                               maxlength="4"
                               value="<?= htmlspecialchars($r->getNumber()) ?>">
                    </div>
                </div>

                <div class="col-md-1"></div>
                <div class="col-md-4">
                    <div class="form-group required ">
                        <label class="control-label">Name *</label>
                        <input type="text" class="form-control <?=$r->hasErrors('name') ? 'is-invalid' : ''?>"
                               name="name"
                               maxlength="64"
                               value="<?= htmlspecialchars($r->getName()) ?>">
                    </div>
                </div>

                <div class="col-md-5"></div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Personen *</label>
                        <input type="number" class="form-control <?=$r->hasErrors('persons') ? 'is-invalid' : ''?>"
                               name="persons"
                               min="1"
                               value="<?= htmlspecialchars($r->getPersons()) ?>">
                    </div>
                </div>

                <div class="col-md-1"></div>
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Preis *</label>
                        <input type="text" class="form-control <?=$r->hasErrors('price') ? 'is-invalide' : '' ?>"
                               name="price" 
                               value="<?= htmlspecialchars($r->getPrice()) ?>">
                    </div>
                </div>

                <div class="col-md-1"></div>
                <div class="col-md-1">
                    <div class="form-group required ">
                        <label class="control-label">Balkon *</label>
                        <input type="hidden" name="balcony" value="0">
                        <input type="checkbox" class="form-control"
                               name="balcony" <?= $r->getBalcony() ? 'checked' : '' ?>>
                    </div>
                </div>

                <div class="col-md-5"></div>
            </div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-success">Erstellen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>