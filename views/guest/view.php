<?php
$title = "Gast anzeigen";
include '../layouts/top.php';
require_once '../../models/Guest.php';

//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();

if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} elseif (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    $g = Guest::get($_GET['id']);
}

if ($g == null) {
    http_response_code(404);
    die();
}
?>

    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id= <?= $g->getId() ?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id= <?= $g->getId() ?>">Löschen</a>
            <a class="btn btn-default" href="index.php">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th>ID</th>
                <td><?= $g->getId() ?></td>
            </tr>
            <tr>
                <th>Name</th>
                <td><?= $g->getName() ?></td>
            </tr>
            <tr>
                <th>E-Mail</th>
                <td><?= $g->getEmail() ?></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>