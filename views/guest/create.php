<?php
$title = "Gast erstellen";
include '../layouts/top.php';

//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();

require_once '../../models/Guest.php';

$g = new Guest();
$message = '';

if (!empty($_POST)) {

    $g->setName($_POST['name'] ?? '');
    $g->setEmail($_POST['email'] ?? '');


    if($g->validateGuest()){
        $message = "<p class='alert alert-success'>Die eingegebenen Daten sind in Ordnung!</p>";
        if ($g->create()) {
            header("Location: update.php?id= " . $g->getId());
            exit();
        }
    }else{
        $message = "<div class='alert alert-danger'><p>Die eingegebenen Daten sind Falsch!</p><ul>";
        foreach ($g->getErrors() as $key => $value){
            $message .= "<li>" . $value . "</li>";
        }
        $message .= "</ul></div>";
    }

}
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>

            <?php echo $message ?>
        </div>

        <form class="form-horizontal" action="create.php" method="post">

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Name *</label>
                        <input type="text" class="form-control <?= $g->hasErrors('name') ? 'is-invalid' : ''?>"
                               name="name"
                               maxlength="30"
                               value="<?= htmlspecialchars($g->getName()) ?>">
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-4">
                    <div class="form-group required ">
                        <label class="control-label">Email *</label>
                        <input type="email" class="form-control <?= $g->hasErrors('email') ? 'is-invalid' : ''?>"
                               name="email" maxlength="20"
                               value="<?= htmlspecialchars($g->getEmail())?>">
                    </div>
                </div>
                <div class="col-md-5"></div>
            </div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-success">Erstellen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>