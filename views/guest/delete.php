<?php
$title = "Gast löschen";
include '../layouts/top.php';

//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();

require_once "../../models/Guest.php";

$id = !empty($_GET['id']) ? $_GET['id'] : 0;

if (!empty($_POST['id'])) {
    Guest::delete($_POST['id']);
    header("Location: ./index.php");
    exit();
} else {
    $g = Guest::get($id);
}

?>

    <div class="container">
        <h2><?= $title ?></h2>

        <form class="form-horizontal" action="delete.php?id= <?= $g->getId() ?>" method="post">
            <input type="hidden" name="id" value="<?= $g->getId() ?>"/>
            <p class="alert alert-error">Wollen Sie den Gast <?= $g->getName() ?> wirklich löschen?</p>
            <div class="form-actions">
                <button type="submit" name="submit" class="btn btn-danger">Löschen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>