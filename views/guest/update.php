<?php
$title = "Gast bearbeiten";
include '../layouts/top.php';

//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();

require_once '../../models/Guest.php';


$m = '';

if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} elseif (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    $g = Guest::get($_GET['id']);
}

if ($g == null) {
    http_response_code(404);
    die();
}

if (!empty($_POST)) {

    $g->setName($_POST['name'] ?? '');
    $g->setEmail($_POST['email'] ?? '');

    if($g->validateGuest()){
        $m = "<p class='alert alert-success'>Die eingegebenen Daten sind in Ordnung!</p>";
        if ($g->update()) {
            header("Location: update.php?id= " . $g->getId());
            exit();
        }
    }else{
        $m = "<div class='alert alert-danger'><p>Die eingegebenen Daten sind Falsch!</p><ul>";
        foreach ($g->getErrors() as $key => $value){
            $m .= "<li>" . $value . "</li>";
        }
        $m .= "</ul></div>";
    }
}
?>

<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
        <?php echo $m ?>
    </div>

    <form class="form-horizontal" action="update.php?id= <?= $g->getId() ?>" method="post">

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Name *</label>
                    <input type="text" class="form-control <?= $g->hasErrors('name') ? 'is-invalid' : '' ?>"
                           name="name"
                           maxlength="20"
                           value="<?= htmlspecialchars($g->getName())  ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group required ">
                    <label class="control-label">Email *</label>
                    <input type="email" class="form-control <?= $g->hasErrors('email') ? 'is-invalid' : '' ?>"
                           name="email"
                           maxlength="20"
                           value="<?= htmlspecialchars($g->getEmail())  ?>">
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="form-group">
            <button type="submit" href="index.php" name="submit" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>
