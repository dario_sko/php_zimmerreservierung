<?php
$title = "Gastverwaltung";
include '../layouts/top.php';

//Überprüfung ob es Datenbank gibt
require_once "../../models/Database.php";
Database::databaseCheck();

?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success">Erstellen <span
                            class="glyphicon glyphicon-plus"></span></a>
            </p>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                require_once '../../models/Guest.php';

                $guests = Guest::getAll();

                foreach ($guests as $g) {
                    echo '<tr>';
                    echo '<td> ' . $g->getId() . ' </td>';
                    echo '<td> ' . $g->getName() . ' </td>';
                    echo '<td> ' . $g->getEmail() . ' </td>';
                    echo '<td><a class="btn btn-info" href="view.php?id= ' . $g->getId() . ' "><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a
                                class="btn btn-primary" href="update.php?id= ' . $g->getId() . ' "><span
                                    class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a
                                class="btn btn-danger" href="delete.php?id= ' . $g->getId() . '"><span
                                    class="glyphicon glyphicon-remove"></span></a>';
                    echo '</td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>