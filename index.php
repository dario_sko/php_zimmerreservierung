<?php
//header('Location: views/room/index.php');
//?>



<?php
require_once "models/Database.php";
$title = "Zimmmerreservierung";

if (isset($_POST['submit'])) {
    header("Location: database/install.php");
}
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title><?= $title ?></title>

    <link rel="shortcut icon" href="css/favicon.ico" type="image/x-icon">
    <link rel="icon" href="css/favicon.ico" type="image/x-icon">

    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<?php
if (Database::connect() == null) { ?>

    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Zimmerreservierung</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

    <div class="container">
        <p>Datenbank für Zimmerreservierung noch nicht vorhanden!</p>

        <form class="form-horizontal" action="index.php" method="post">
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-success">Datenbank installieren</button>
            </div>
        </form>
    </div>
    <?php
} else {
    header("Location: views/reservation/index.php");
}


include 'views/layouts/bottom.php';
?>
